﻿using ParamParser;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ArgsParser
{
    /// <summary>
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
    public class ParamAttribute : Attribute
    {
        public ParamAttribute(string Key, string FullName)
        {
            this.Key = Key;
            this.FullName = FullName;
        }

        /// <summary>
        /// Key of argument
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Full name of argument
        /// </summary>
        public string FullName
        {
            get { return _fullName; }
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new ArgumentNullException("FullName");
                if (value.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Count() > 1)
                    throw new ArgumentException("FullName attribute must be a single word!");
                _fullName = value;
            }
        }
        private string _fullName;

        /// <summary>
        /// The argument description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// An example of using the argument
        /// </summary>
        public string Example { get; set; }
    }
    

    /// <summary>
    /// Specifies that this property can be set from the input parameters
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class PropertyParamAttribute : ParamAttribute
    {
        /// <summary>
        /// The value that is set if not specified
        /// </summary>
        public object DefaultValue { get; set; }

        public PropertyParamAttribute(string Key, string FullName)
            : base(Key, FullName)
        {

        }
    }

    /// <summary>
    /// Indicates that this method executes if the key is encountered in the input parameters
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
    public class MethodParamAttribute : ParamAttribute
    {
        /// <summary>
        /// Execution priority. 0 - max priority
        /// </summary>
        public int Priority
        {
            get { return _priority; }
            set { _priority = value; }
        }
        protected int _priority = int.MaxValue;

        public MethodParamAttribute(string Key, string FullName)
            : base(Key, FullName)
        {

        }
    }

    /// <summary>
    /// Interface to display the description and examples
    /// </summary>
    public interface IArgsHelpViewer
    {
        string GetParamHelp();
        string GetParamExample();
    }

    /// <summary>
    /// Базовый класс для хранения входных параметров. Позволяет отображать справку и примеры
    /// </summary>
    public class ArgsOptionsBase : IArgsHelpViewer
    {
        /// <summary>
        /// Displays help on all the above parameters
        /// </summary>
        /// <returns></returns>
        public virtual string GetParamHelp()
        {
            var props = this.GetType().GetProperties().Where(x => x.GetCustomAttribute<ParamAttribute>() != null);

            var methods = this.GetType().GetMethods().Where(x=>x.GetCustomAttribute<ParamAttribute>() != null);

            var showList = new List<Tuple<string, string, string>>();

            foreach (var p in props)
            {
                var atr = p.GetCustomAttribute<PropertyParamAttribute>();
                if (atr == null) continue;

                showList.Add(new Tuple<string, string, string>(atr.Key, atr.Description, atr.Example));
            }

            foreach (var m in methods)
            {
                var atr = m.GetCustomAttribute<MethodParamAttribute>();
                if (atr == null) continue;

                showList.Add(new Tuple<string, string, string>(atr.Key, atr.Description, atr.Example));
            }

            StringBuilder paramsSB = new StringBuilder();

            paramsSB.AppendLine(" Parameters:");

            foreach (var k in showList.OrderBy(x => x.Item1))
            {
                paramsSB.AppendLine(String.Format("\t{0}{1, -12}\t{2}", ArgsManager.KeyPrefix, k.Item1, k.Item2));
            }

            return paramsSB.ToString();
        }

        /// <summary>
        /// Shows the examples on all the above parameters
        /// </summary>
        /// <returns></returns>
        public string GetParamExample()
        {
            var props = this.GetType().GetProperties().Where(x => x.GetCustomAttribute<ParamAttribute>() != null);

            var methods = this.GetType().GetMethods().Where(x => x.GetCustomAttribute<ParamAttribute>() != null);

            var showList = new List<Tuple<string, string, string>>();

            foreach (var p in props)
            {
                var atr = p.GetCustomAttribute<PropertyParamAttribute>();
                if (atr == null) continue;

                showList.Add(new Tuple<string, string, string>(atr.Key, atr.Description, atr.Example));
            }

            foreach (var m in methods)
            {
                var atr = m.GetCustomAttribute<MethodParamAttribute>();
                if (atr == null) continue;

                showList.Add(new Tuple<string, string, string>(atr.Key, atr.Description, atr.Example));
            }

            StringBuilder exampSB = new StringBuilder();

            exampSB.AppendLine(" Examples:");

            foreach (var k in showList.OrderBy(x => x.Item1))
            {
                //exampSB.AppendLine(String.Format("> {0} {1}", Process.GetCurrentProcess().ProcessName, k.Item3));
                exampSB.AppendLine(String.Format("\t> {0}", k.Item3));
            }

            return exampSB.ToString();
        }
    }

    /// <summary>
    /// Allows to parse input parameters
    /// </summary>
    public class ArgsManager
    {
        /// <summary>
        /// Prefix for argument key
        /// </summary>
        public static string KeyPrefix
        {
            get { return _keyPrefix; }
            set
            {
                if (value != null && !string.IsNullOrEmpty(value.Trim()))
                {
                    _keyPrefix = value;
                    return;
                }
                throw new ArgumentNullException("KeyPrefix");
            }
        }
        private static string _keyPrefix = "-";

        public ArgsManager()
        {

        }

        /// <summary>
        /// Parse input arguments and invoke methods
        /// </summary>
        /// <typeparam name="T">The type of object that stores the parameters</typeparam>
        /// <param name="args">Array of arguments</param>
        /// <returns></returns>
        public static T ParseArgs<T>(string[] args)
        {
            return ParseArgs<T>(args, true);
        }

        /// <summary>
        /// Parse input arguments
        /// </summary>
        /// <typeparam name="T">The type of object that stores the parameters</typeparam>
        /// <param name="args">Array of arguments</param>
        /// <param name="autoInvokeMethods">Invoke method </param>
        /// <returns>Object with parsed arguments</returns>
        public static T ParseArgs<T>(string[] args, bool autoInvokeMethods)
        {
            var option = Activator.CreateInstance<T>();

            var dArgs = ArgsToDictionary(args);

            // Get and pars properties
            var props = typeof(T).GetProperties();              //.Where(x => x.GetCustomAttributes<ParamAttribute>().Count() != 0);
            ParseProperties<T>(option, dArgs, props);

            // Get and parse methods
            var methods = typeof(T).GetMethods();               //.Where(x => x.GetCustomAttributes<ParamAttribute>().Count() != 0);
            var invokeMethods = ParseMethods<T>(option, methods, dArgs);

            // Invoke parsed methods
            if (autoInvokeMethods)
            {
                foreach (var m in invokeMethods.OrderBy(x => x.Key))
                {
                    m.Value.Invoke();
                }
            }

            return option;
        }
        
        private static Dictionary<int, Action> ParseMethods<T>(T option, IEnumerable<MethodInfo> methods, Dictionary<string, string> dArgs)
        {
            var invokeMethods = new Dictionary<int, Action>();
            foreach (var m in methods)
            {
                var atr = m.GetCustomAttribute<MethodParamAttribute>();
                if (atr == null) continue;

                if (dArgs.ContainsKey(atr.Key) || dArgs.ContainsKey(atr.FullName))
                {
                    var mParams = m.GetParameters();
                    var mParamVal = (dArgs[atr.Key] ?? dArgs[atr.FullName]).Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    if (mParams.Count() != mParamVal.Count())
                        throw new Exception("The count of input parameters does not match the method signature!");

                    var resParams = new List<object>(mParams.Count());
                    for (int i = 0; i < mParams.Count(); i++)
                    {
                        resParams.Add(Convert.ChangeType(mParamVal[i], mParams[i].ParameterType));
                    }

                    invokeMethods.Add(atr.Priority, new Action(() => { m.Invoke(option, resParams.ToArray()); }));
                }
            }
            return invokeMethods;
        }

        private static void ParseProperties<T>(T option, Dictionary<string, string> dArgs, PropertyInfo[] props)
        {
            foreach (var p in props)
            {
                var atr = p.GetCustomAttribute<PropertyParamAttribute>();

                if (atr == null) continue;

                if (dArgs.ContainsKey(atr.Key) || dArgs.ContainsKey(atr.FullName))
                {
                    if (p.PropertyType == typeof(bool)) // bool
                    {
                        SetPropVal(option, true, p);
                        continue;
                    }
                    else
                    {
                        var key = dArgs.ContainsKey(atr.Key) && !string.IsNullOrEmpty(dArgs[atr.Key])
                                      ? dArgs[atr.Key] : null;
                        var name = dArgs.ContainsKey(atr.FullName) && !string.IsNullOrEmpty(dArgs[atr.FullName])
                                        ? dArgs[atr.FullName] : null;

                        if (p.PropertyType.IsArray) // Collection
                        {
                            var val = key ?? name;

                            SetPropVal(option, val.Split(' ') ?? atr.DefaultValue, p);
                            continue;
                        }
                        else // other types
                        {
                            SetPropVal(option, key ?? name ?? atr.DefaultValue, p);
                        }
                    }
                }
                else
                {
                    if (p.PropertyType == typeof(bool))
                    {
                        SetPropVal(option, atr.DefaultValue ?? false, p);
                    }
                }
            }
        }

        // controlled set value
        private static void SetPropVal(object option, object val, PropertyInfo p)
        {
            if (p.CanWrite && (val) != null)
            {
                p.SetValue(option, Convert.ChangeType(val, p.PropertyType, CultureInfo.InvariantCulture));
            }
        }

        /// <summary>
        /// Get input argument value by key
        /// </summary>
        /// <param name="argKey">Key of argument</param>
        /// <param name="args">Array of arguments</param>
        /// <returns>String value of argument</returns>
        public static string GetParamValue(string argKey, string[] args)
        {
            var dArgs = ArgsToDictionary(args);
            return dArgs != null && dArgs.ContainsKey(argKey) ? dArgs[argKey] : null;
        }

        /// <summary>
        /// Get input arguments as a dictionary [key - value]
        /// </summary>
        /// <param name="args">Array of arguments</param>
        /// <returns>Dictionary<argKey, argValue></returns>
        public static Dictionary<string, string> ArgsToDictionary(string[] args)
        {
            var checkList = args.Where(x => x.StartsWith(ArgsManager.KeyPrefix)).ToList();
            if (checkList.Distinct().Count() != checkList.Count)
                throw new Exception("Args list contains non unique keys");

            Dictionary<string, string> _args = new Dictionary<string, string>();

            if (args == null) return _args;

            string argKey = String.Empty;

            foreach (string arg in args)
            {
                //if argument is a key
                if (arg.StartsWith(ArgsManager.KeyPrefix))
                {
                    //push key
                    argKey = arg.Replace(ArgsManager.KeyPrefix, String.Empty);
                    _args.Add(argKey, String.Empty);
                }
                //push value
                else
                {
                    _args[argKey] = string.IsNullOrEmpty(_args[argKey]) ? arg : _args[argKey] + " " + arg;
                }
            }

            return _args;
        }
    }
}
